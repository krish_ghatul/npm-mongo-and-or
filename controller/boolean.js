const b = require("../services/boolean-query-service.js");
const v = require("../services/validation-service.js");

class BooleanSearch {
    static getQuery(req, res) {
        let q = '(krish AND amita OR V)';
        if(!v.validateInfix(q)) {
        res.send("Invalid");
        return;
        }
        let ak = b.query(q, 'groups', 'array');
        res.send({query: ak});
    }
}
module.exports = BooleanSearch;