const express = require('express');
const router = express.Router();
const booleanS = require("../controller/boolean.js");

router.get('/gk', booleanS.getQuery);

module.exports = router;
