//import { BooleanQueryService } from "./services/boolean-query-service.js"
const BooleanQueryService = require("./services/boolean-query-service.js");

module.exports = {
  mongoBoolean: BooleanQueryService
};
